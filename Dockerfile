FROM alpine:latest

MAINTAINER Rustem Uspanov <uspanov.r@gmail.com>

RUN apk update && \
  apk add postgresql-client && \
  apk add bash && \
  apk add openssh-client && \
  apk add git && \
  apk add python && \
  apk add make gcc g++ paxctl curl && \
  apk add py2-pip && \
  pip install PyYAML && \
  pip install python-gitlab && \
  rm -rf /var/cache/apk/*